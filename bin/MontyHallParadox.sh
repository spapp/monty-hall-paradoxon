#!/bin/sh
# Monty Hall Paradox test.
#
# @author Sándor Papp
# @link <https://bitbucket.org/spapp/monty-hall-paradoxon>
# @link <http://spappsite.hu/blog/monty-hall-paradoxon>
#
# @param   $1   number of attempts (default: 100)
# @param   $2   number of attempts blocks (default: 10)

REPEAT=${2:-10}
MAX_COUNT=$(expr ${1:-100} / ${REPEAT})
ALL=0
TAB='    '
TAB2=${TAB}
SEP='------------------------'

echo 'Monty Hall Paradox test'
echo ${SEP}
echo "Started ${REPEAT} × ${MAX_COUNT} tests"
echo

for i in $(seq 1 ${REPEAT});
do
    VICTORY=0;

    for j in $(seq 1 ${MAX_COUNT});
    do
        CAR=$((RANDOM%3+1))
        TEST=$((RANDOM%3+1))

        # If the car is not equal to the test then we win
        # because we always changing for the tip
        if [ ${CAR} -ne ${TEST} ]
        then
            VICTORY=$(expr ${VICTORY} + 1)
        fi
    done

    ODDS=$(expr $(expr ${VICTORY} '*' 100) / ${MAX_COUNT})
    ALL=$(expr ${ALL} + ${ODDS})

    if [ 2 -eq $(expr length ${i}) ]
    then
        TAB2='   '
    fi

    echo "${TAB}${i}${TAB2}-${TAB}${ODDS} %"
done

echo ${SEP}
echo "${TAB}${TAB}${TAB}  $(expr ${ALL} / ${REPEAT}) %"
